import Pages.LoginPage;
import org.testng.annotations.Test;

import static Util.StringUtils.randomString;

public class Test_10_CorrectAddingCharacteristicProcess_CheckDashboardPage extends SeleniumTestBase {

    @Test

    public void addingNewCharacteristicTest_CheckDashboardPage() {

        String processName = "DEMO PROJECT";
        String characteristicName = randomString(8) + "_test";
        String lowLimit = "5";
        String upLimit = "10";

        new LoginPage(driver)
                .loginToHomePage(config.getEmail(), config.getPassword())
                .goToCharacteristicPage()
                .createNewCharacteristic()
                .selectProcess(processName)
                .insertCharacteristicName(characteristicName)
                .insertLowerLimit(lowLimit)
                .insertUpperLimit(upLimit)
                .createCharacteristic()
                .goToDashboardPage()
                .assertCharacteristicIsDisplayedOnDashboard(processName, characteristicName);
    }
}