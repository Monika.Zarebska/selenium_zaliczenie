import Pages.LoginPage;
import org.testng.annotations.Test;

import static Util.StringUtils.randomString;

public class Test_7_CorrectAddingNewProcess_CheckDashboardPage extends SeleniumTestBase {

    @Test
    public void addingNewProcessTest_CheckDashboardPage() {

        String processName = randomString(6) + "_Test";

        new LoginPage(driver)
                .loginToHomePage(config.getEmail(), config.getPassword())
                .goToProcessesPage()
                .addNewProcess()
                .insertProcessName(processName)
                .createProcess()
                .goToDashboardPage()
                .assertProcessIsDisplayedOnDashboardPage(processName);
    }
}