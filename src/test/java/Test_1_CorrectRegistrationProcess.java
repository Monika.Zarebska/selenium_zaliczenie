import Pages.LoginPage;
import org.testng.annotations.Test;

import static Util.StringUtils.randomString;

public class Test_1_CorrectRegistrationProcess extends SeleniumTestBase {

    @Test
    public void CorrectRegistrationProcessTest() {

        String randomEmail = randomString(7) +
                "@" + randomString(4) +
                "." + randomString(3);

        String password = "JakiesH@slo1";

        new LoginPage(driver)
                .goToRegistrationPage()
                .insertEmail(randomEmail)
                .insertPassword(password)
                .confirmPassword(password)
                .clickRegistrationBtn()
                .assertRegistrationProcess(randomEmail);
    }
}