import Pages.LoginPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Test_3_IncorrectRegistrationProcess_WrongPassword extends SeleniumTestBase {

    @DataProvider
    public Object[][] wrongPasswordWithExpectedError() {
        return new Object[][]{
                {"h@slo1", "Passwords must have at least one uppercase" },
                {"H@sloo", "Passwords must have at least one digit" },
                {"Haslo1", "Passwords must have at least one non alphanumeric character" },
                {"H@SLO1", "Passwords must have at least one lowercase" }, // dodałam jeszcze brak małych liter
                {"haslo", "Password must be at least 6 and at max 100 characters" }, // dodałam tez test na za krótkie haslo

        };
    }

    @Test(dataProvider = "wrongPasswordWithExpectedError")
    public void wrongPasswordIncorrectRegistrationTest(String wrongPassword, String expectedError) {

        new LoginPage(driver)
                .goToRegistrationPage()
                .insertEmail("Test@Test.com")
                .insertPassword(wrongPassword)
                .confirmPassword(wrongPassword)
                .clickRegistrationBtnWithError()
                .assertRegistrationErrorIsShown(expectedError);
    }

    @Test
    public void wrongPassword3Errors() { // taki dodatkowy test czy wyświetlą się trzy powiadomienia

        new LoginPage(driver)
                .goToRegistrationPage()
                .insertEmail("Test@Test.com")
                .insertPassword("hasloo")
                .confirmPassword("hasloo")
                .clickRegistrationBtnWithError()
                .assertRegistrationErrorIsShown("must have at least one uppercase")
                .assertRegistrationErrorIsShown("must have at least one digit")
                .assertRegistrationErrorIsShown("must have at least one non alphanumeric character");
    }
}