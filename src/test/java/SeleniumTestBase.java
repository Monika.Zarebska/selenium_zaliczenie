import Config.Config;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class SeleniumTestBase {

    protected WebDriver driver;
    protected Config config;

    @BeforeMethod
    public void startDriverAndGetAppUrl() {
        config = new Config();
        System.setProperty(config.getDriver(), config.getDriverPath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(config.getApplicationUrl());
    }

    @AfterMethod
    public void quitDriver() {
        driver.quit();
    }
}