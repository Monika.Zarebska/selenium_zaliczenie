import Pages.LoginPage;
import org.testng.annotations.Test;

public class Test_2_IncorrectRegistrationProcess_InvalidConfirmationPassword extends SeleniumTestBase {

    @Test
    public void incorrectRegistrationProcessWrongPasswordTest() {

        new LoginPage(driver)
                .goToRegistrationPage()
                .insertEmail("Test@Test.com")
                .insertPassword("H@slo1")
                .confirmPassword("InneH@slo1")
                .clickRegistrationBtnWithError()
                .assertErrorIsShown();
    }
}