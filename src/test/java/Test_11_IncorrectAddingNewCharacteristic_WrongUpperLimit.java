import Pages.LoginPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.UUID;

import static Util.StringUtils.randomString;

public class Test_11_IncorrectAddingNewCharacteristic_WrongUpperLimit extends SeleniumTestBase {

    @DataProvider
    public Object[][] incorrectUpLimit(){
        return new Object[][]{
                {"", "The value '' is invalid" },
                {"Ab", "The value 'Ab' is not valid for Upper Specification Limit" },
        };
    }

    @Test(dataProvider = "incorrectUpLimit")
    public void incorrectAddingNewCharacteristic_NoUpperLimit(String incorrectUpLimit, String expectedError) {

        String processName = "DEMO PROJECT";
        String characteristicName = randomString(8) + "_test";
        String lowLimit = "5";

        new LoginPage(driver)
                .loginToHomePage(config.getEmail(), config.getPassword())
                .goToCharacteristicPage()
                .createNewCharacteristic()
                .selectProcess(processName)
                .insertCharacteristicName(characteristicName)
                .insertLowerLimit(lowLimit)
                .insertUpperLimit(incorrectUpLimit)
                .createCharacteristicPageWithError()
                    .assertErrorIsShown(expectedError)
                .goToCharacteristicPage()
                    .assertCharacteristicInNotShown(characteristicName);
    }
}