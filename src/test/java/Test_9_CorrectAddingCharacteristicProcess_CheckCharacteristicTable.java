import Pages.LoginPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.UUID;

import static Util.StringUtils.randomString;

public class Test_9_CorrectAddingCharacteristicProcess_CheckCharacteristicTable extends SeleniumTestBase {

    @DataProvider
    public Object[][] upLimit(){
        return new Object[][]{
                {"10"},
                {"10.3"},
                {"10,3"}, //znaleziony bug
        };
    }

    @Test(dataProvider = "upLimit")
    public void addingNewCharacteristicTest_CheckCharacteristicTable(String upLimit) {

        String processName = "DEMO PROJECT";
        String characteristicName = randomString(8) + "_test";
        String lowLimit = "5";

        new LoginPage(driver)
                .loginToHomePage(config.getEmail(), config.getPassword())
                .goToCharacteristicPage()
                .createNewCharacteristic()
                .selectProcess(processName)
                .insertCharacteristicName(characteristicName)
                .insertLowerLimit(lowLimit)
                .insertUpperLimit(upLimit)
                .createCharacteristic()
                .assertCharacteristicIsCreated(processName, characteristicName, lowLimit, upLimit, "");
    }
}