import Pages.LoginPage;
import org.testng.annotations.Test;

public class Test_4_CorrectLoginProcess extends SeleniumTestBase {

    @Test
    public void CorrectLoginTest() {
        new LoginPage(driver)
                .insertEmail(config.getEmail())
                .insertPassword(config.getPassword())
                .clickLoginBtn()
                .assertLoginProcess();
    }
}