import Pages.LoginPage;
import org.testng.annotations.Test;

import static Util.StringUtils.randomString;

public class Test_6_CorrectAddingNewProcess_CheckProcessesTable extends SeleniumTestBase {

    @Test
    public void addingNewProcessTest_CheckProcessesTable() {

        String processName = randomString(8) + "_test";

        new LoginPage(driver)
                .loginToHomePage(config.getEmail(), config.getPassword())
                .goToProcessesPage()
                .addNewProcess()
                .insertProcessName(processName)
                .createProcess()
                .assertProcessIsPresent(processName, "", "");
    }

    @Test
    public void addingNewProcessTest_CheckProcessesTable_WithDescAndNotes() {

        String processName = randomString(8) + "_test";
        String processDescription = randomString(10) + " test";
        String processNotes = randomString(15) + " test";

        new LoginPage(driver)
                .loginToHomePage(config.getEmail(), config.getPassword())
                .goToProcessesPage()
                .addNewProcess()
                .insertProcessName(processName)
                .insertDescription(processDescription)
                .insertNotes(processNotes)
                .createProcess()
                .assertProcessIsPresent(processName, processDescription, processNotes);
    }
}