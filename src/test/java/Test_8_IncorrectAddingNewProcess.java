import Pages.LoginPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Test_8_IncorrectAddingNewProcess extends SeleniumTestBase {

    @DataProvider
    public Object[][] wrongNames() {
        return new Object[][]{
                {"ab"},
                {"trzydziesciJedenSymboliWNazwie!"}
        };
    }

    @Test(dataProvider = "wrongNames")
    public void incorrectAddingNewProcessTest(String wrongProcessName) {

        String expectedErrorMessage = "Name must be a string with a minimum length of 3 and a maximum length of 30";

        new LoginPage(driver)
                .loginToHomePage(config.getEmail(), config.getPassword())
                .goToProcessesPage()
                .addNewProcess()
                .insertProcessName(wrongProcessName)
                .createProcessWithError()
                    .assertCreateProcessError(expectedErrorMessage)
                .backToProcessPage()
                    .assertProcessInNotShown(wrongProcessName);
    }
}