import Pages.LoginPage;
import org.testng.annotations.Test;

public class Test_5_HomePage_Menu extends SeleniumTestBase {

    @Test
    public void homePageMenuTestCorrectUrl() {
        new LoginPage(driver)
                .loginToHomePage(config.getEmail(),config.getPassword())
                .goToCharacteristicPage()
                    .assertCharacteristicPageUrl(config.getCharacteristicsUrl())
                    .assertCharacteristicHeaderIsDisplayed()
                .goToDashboardPage()
                    .assertDashboardUrl(config.getDashboardUrl())
                    .assertDashboardPageIsDisplayed()
                .goToProcessesPage()
                    .assertProcessesUrl(config.getProcessesPageUrl())
                    .assertProcessHeaderIsDisplayed();
    }
}