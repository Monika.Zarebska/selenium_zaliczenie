import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class Test_1_CorrectRegistrationProcess_basic extends SeleniumTestBase {

    @Test
    public void CorrectRegistrationProcessTest() {

        String randomEmail = (UUID.randomUUID()
                .toString().substring(0, 7) + "@"
                + (UUID.randomUUID().toString().substring(0, 4))
                + "." + (UUID.randomUUID().toString().substring(0, 3)));

        WebElement goToRegisterPageBtn = driver.findElement(By.xpath("//a[contains (text(), 'Register as a new user')]"));
        goToRegisterPageBtn.click();

        WebElement emailTxt = driver.findElement(By.cssSelector("#Email"));
        emailTxt.sendKeys(randomEmail);
        WebElement passwordTxt = driver.findElement(By.cssSelector("#Password"));
        passwordTxt.sendKeys("JakiesHaslo@1");
        WebElement confirmPasword = driver.findElement(By.cssSelector("#ConfirmPassword"));
        confirmPasword.sendKeys("JakiesHaslo" +
                "@1");
        WebElement registerBtn = driver.findElement(By.xpath("//button[contains (text(),'Register')]"));
        registerBtn.click();

        WebElement welcomeElm = driver.findElement(By.cssSelector(".profile_info>h2"));
        Assert.assertTrue(welcomeElm.isDisplayed());
        Assert.assertTrue(welcomeElm.getText().contains("Welcome"));
    }
}