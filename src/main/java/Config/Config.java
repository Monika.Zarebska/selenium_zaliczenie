package Config;

import java.io.InputStream;
import java.util.Properties;

public class Config {

    private Properties properties;

    public Config() {
        properties = getProperties();
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
            properties.load(inputStream);
        } catch (Exception e) {
            throw new RuntimeException("Cannot load properties file: " + e);
        }
        return properties;
    }

    public String getDriver() {
        return properties.getProperty("chrome.driver");
    }

    public String getDriverPath() {
        return properties.getProperty("chrome.driver.path");
    }

    public String getApplicationUrl() {
        return properties.getProperty("application.url");
    }

    public String getEmail() {
        return properties.getProperty("application.username");
    }

    public String getPassword() {
        return properties.getProperty("application.password");
    }

    public String getProcessesPageUrl() {
        return properties.getProperty("page.processes.url");
    }

    public String getDashboardUrl() {
        return properties.getProperty("page.dashboard.url");
    }

    public String getCharacteristicsUrl() {
        return properties.getProperty("page.characteristics.url");
    }
}