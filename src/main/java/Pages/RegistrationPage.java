package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class RegistrationPage {

    protected WebDriver driver;

    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailField;

    @FindBy(id = "Password")
    private WebElement passwordField;

    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPasswordField;

    @FindBy(xpath = "//button[contains (text(),'Register')]")
    private WebElement registrationBtn;

    @FindBy(id = "ConfirmPassword-error")
    private WebElement confirmErroroPasswordError;

    @FindBy(css = ".validation-summary-errors>ul>li")
    private List<WebElement> registrationErrors;


    public RegistrationPage insertEmail(String email) {
        emailField.clear();
        emailField.sendKeys(email);
        return this;
    }

    public RegistrationPage insertPassword(String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
        return this;
    }

    public RegistrationPage confirmPassword(String confirmPassword) {
        confirmPasswordField.clear();
        confirmPasswordField.sendKeys(confirmPassword);
        return this;
    }

    public HomePage clickRegistrationBtn() {
        registrationBtn.click();
        return new HomePage(driver);
    }

    public RegistrationPage clickRegistrationBtnWithError() {
        registrationBtn.click();
        return this;
    }


    public RegistrationPage assertErrorIsShown() {
        Assert.assertTrue(confirmErroroPasswordError.isDisplayed());
        Assert.assertEquals(confirmErroroPasswordError.getText(), "The password and confirmation password do not match.");
        return this;
    }

    public RegistrationPage assertRegistrationErrorIsShown(String expectedError) {
        boolean doesErrorExist = registrationErrors
                .stream()
                .anyMatch(error -> error.getText().contains(expectedError));
        Assert.assertTrue(doesErrorExist, "'" + expectedError + "' has not been shown");
        return this;
    }
}