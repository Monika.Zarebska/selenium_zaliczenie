package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class CharacteristicPage extends HomePage {

    public CharacteristicPage(WebDriver driver) {
        super(driver);
    }

    private static final String XPATH_TEMPLATE_FOR_CHARACTERISTIC_ROW = "//td[text()='%s']/..";

    @FindBy(css = ".title_left h3")
    private WebElement characteristicsHeader;

    @FindBy(linkText = "Add new characteristic")
    private WebElement addNewCharacteristicBtn;

    public CreateCharacteristicPage createNewCharacteristic() {
        addNewCharacteristicBtn.click();
        return new CreateCharacteristicPage(driver);
    }


    public CharacteristicPage assertCharacteristicPageUrl(String characteristicPageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), characteristicPageUrl);
        return this;
    }

    public CharacteristicPage assertCharacteristicHeaderIsDisplayed() {
        Assert.assertTrue(characteristicsHeader.isDisplayed());
        Assert.assertEquals(characteristicsHeader.getText(), "Characteristics");
        return this;
    }

    public CharacteristicPage assertCharacteristicIsCreated(String processName, String characteristicName, String lowLimit, String upLimit, String histogramBinCount) {
        String xpathForCharacteristicRow = String.format(XPATH_TEMPLATE_FOR_CHARACTERISTIC_ROW, characteristicName);
        WebElement characteristicRow = driver.findElement(By.xpath(xpathForCharacteristicRow));
        String processNameFromTable = characteristicRow.findElement(By.xpath("./td[1]")).getText();
        String lowLimitFromTable = characteristicRow.findElement(By.xpath("./td[3]")).getText();
        String upLimitFromTable = characteristicRow.findElement(By.xpath("./td[4]")).getText();
        String histogramBinCountFromTable = characteristicRow.findElement(By.xpath("./td[5]")).getText();

        Assert.assertEquals(processNameFromTable, processName);
        Assert.assertEquals(lowLimitFromTable, lowLimit);
        Assert.assertEquals(upLimitFromTable, upLimit);
        Assert.assertEquals(histogramBinCountFromTable, histogramBinCount);

        return this;
    }

    public CharacteristicPage assertCharacteristicInNotShown(String characteristicName) {
        String xpathForCharacteristicRow = String.format(XPATH_TEMPLATE_FOR_CHARACTERISTIC_ROW, characteristicName);
        List<WebElement> characteristicRows = driver.findElements(By.xpath(xpathForCharacteristicRow));
        return this;
    }
}