package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class DashboardPage extends HomePage {

    public DashboardPage(WebDriver driver) {
        super(driver);
    }


    private static final String CREATE_YOUR_FIRST_PROCESS_LINK_TEXT = "Create your first process";

    private static final String XPATH_FOR_PROCESS_CHARACTERISTIC_TEMPLATE = "//div[@class='x_title']/h2[text()='%s']/../..//p";

    @FindBy(xpath = "//div[@class='x_title']/h2")
    private List<WebElement> processesOnDashboard;


    public DashboardPage assertDashboardUrl(String dashboardPageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), dashboardPageUrl);
        return this;
    }

    public DashboardPage assertDashboardPageIsDisplayed() {
        Assert.assertTrue(driver.findElements(By.linkText(CREATE_YOUR_FIRST_PROCESS_LINK_TEXT)).size() == 1
                || processesOnDashboard.size() > 0);
        return this;
    }// uwzględniłam że ktoś usunie Demo Project doda inne procesy

    public DashboardPage assertProcessIsDisplayedOnDashboardPage(String processName) {
        for (WebElement processElement : processesOnDashboard) {
            if (processElement.getText().equals(processName)) {
                return this;
            }
        }
        Assert.fail("No process on Dashboard with name: " + processName);
        return this;
    }

    public DashboardPage assertCharacteristicIsDisplayedOnDashboard(String processName, String characteristicName) {

        String xpathForProcessCharacteristics = String.format(XPATH_FOR_PROCESS_CHARACTERISTIC_TEMPLATE, processName);
        List<WebElement> processCharacteristics = driver.findElements(By.xpath(xpathForProcessCharacteristics));

        for (WebElement characteristic : processCharacteristics) {
            if (characteristic.getText().equals(characteristicName)) {
                return this;
            }
        }
        Assert.fail("No characteristic '" + characteristicName + "' on process: '" + processName + "' on Dashboard Page");
        return this;
    }
}