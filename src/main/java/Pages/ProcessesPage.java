package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class ProcessesPage extends HomePage {

    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    private static final String XPATH_TEMPLATE_FOR_PROCESS_ROW = "//td[text()='%s']/..";


    @FindBy(css = ".title_left h3")
    private WebElement processesHeader;

    @FindBy(linkText = "Add new process")
    private WebElement addNewProcessBtn;


    public CreateProcessPage addNewProcess() {
        addNewProcessBtn.click();
        return new CreateProcessPage(driver);
    }


    public ProcessesPage assertProcessesUrl(String processesPageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), processesPageUrl);
        return this;
    }

    public ProcessesPage assertProcessHeaderIsDisplayed() {
        Assert.assertTrue(processesHeader.isDisplayed());
        Assert.assertEquals(processesHeader.getText(), "Processes");
        return this;
    }

    public ProcessesPage assertProcessIsPresent(String processName, String processDescription, String processNotes) {
        String xpathForProcessRow = String.format(XPATH_TEMPLATE_FOR_PROCESS_ROW, processName);
        WebElement processRow = driver.findElement(By.xpath(xpathForProcessRow));
        String descriptionFromTable = processRow.findElement(By.xpath("./td[2]")).getText();
        String notesFromTable = processRow.findElement(By.xpath("./td[3]")).getText();

        Assert.assertEquals(descriptionFromTable, processDescription);
        Assert.assertEquals(notesFromTable, processNotes);

        return this;
    }

    public ProcessesPage assertProcessInNotShown(String processName) {
        String xpathForProcessRow = String.format(XPATH_TEMPLATE_FOR_PROCESS_ROW, processName);
        List<WebElement> processRows = driver.findElements(By.xpath(xpathForProcessRow));

        Assert.assertEquals(processRows.size(), 0);
        return this;
    }
}