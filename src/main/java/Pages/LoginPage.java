package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//a[contains (text(), 'Register as a new user')]")
    private WebElement goToRegisterPageBtn;

    @FindBy(id = "Email")
    private WebElement emailField;

    @FindBy(id = "Password")
    private WebElement passwordField;

    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;


    public RegistrationPage goToRegistrationPage() {
        goToRegisterPageBtn.click();
        return new RegistrationPage(driver);
    }

    public LoginPage insertEmail(String email) {
        emailField.clear();
        emailField.sendKeys(email);
        return this;
    }

    public LoginPage insertPassword(String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
        return this;
    }

    public HomePage clickLoginBtn() {
        loginBtn.click();
        return new HomePage(driver);
    }

    public HomePage loginToHomePage(String email, String password){
        insertEmail(email);
        insertPassword(password);
        clickLoginBtn();
        return  new HomePage(driver);
    }
}