package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateCharacteristicPage extends HomePage{

    protected WebDriver driver;

    public CreateCharacteristicPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(id = "ProjectId")
    private WebElement processName;

    @FindBy(id = "Name")
    private WebElement characteristicName;

    @FindBy(id = "LowerSpecificationLimit")
    private WebElement lowerSpecificationLimit;

    @FindBy(id = "UpperSpecificationLimit")
    private WebElement upperSpecificationLimit;

    @FindBy(css = "input[value=Create]")
    private WebElement createBtn;

    @FindBy(css = ".field-validation-error")
    private WebElement errors;


    public CreateCharacteristicPage selectProcess(String process) {
        new Select(processName).selectByVisibleText(process);
        return this;
    }

    public CreateCharacteristicPage insertCharacteristicName(String name) {
        characteristicName.clear();
        characteristicName.sendKeys(name);
        return this;
    }

    public CreateCharacteristicPage insertLowerLimit(String lowLimit) {
        lowerSpecificationLimit.click();
        lowerSpecificationLimit.sendKeys(lowLimit);
        return this;
    }

    public CreateCharacteristicPage insertUpperLimit(String upLimit) {
        upperSpecificationLimit.click();
        upperSpecificationLimit.sendKeys(upLimit);
        return this;
    }

    public CharacteristicPage createCharacteristic() {
        createBtn.click();
        return new CharacteristicPage(driver);
    }

    public CreateCharacteristicPage createCharacteristicPageWithError(){
        createBtn.click();
        return this;
    }


    public CreateCharacteristicPage assertErrorIsShown(String expectedError){
        Assert.assertTrue(errors.isDisplayed());
        Assert.assertTrue(errors.getText().contains(expectedError));
        return this;
    }
}