package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CreateProcessPage {

    protected WebDriver driver;

    public CreateProcessPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(id = "Name")
    private WebElement processNameField;

    @FindBy(id = "Description")
    private WebElement descriptionField;

    @FindBy(id = "Notes")
    private WebElement notesField;

    @FindBy(css = "input[value=Create]")
    private WebElement createBtn;

    @FindBy(css = ".field-validation-error")
    private WebElement errorInProcessName;

    @FindBy (linkText ="Back to List")
    private WebElement backToListBtn;


    public CreateProcessPage insertProcessName(String processName) {
        processNameField.sendKeys(processName);
        return this;
    }

    public CreateProcessPage insertDescription(String description) {
        descriptionField.sendKeys(description);
        return this;
    }

    public CreateProcessPage insertNotes(String notes) {
        notesField.sendKeys(notes);
        return this;
    }

    public ProcessesPage createProcess() {
        createBtn.click();
        return new ProcessesPage(driver);
    }

    public CreateProcessPage createProcessWithError() {
        createBtn.click();
        return this;
    }

    public ProcessesPage backToProcessPage(){
        backToListBtn.click();
        return new ProcessesPage(driver);
    }


    public CreateProcessPage assertCreateProcessError(String expectedError) {
        Assert.assertTrue(errorInProcessName.isDisplayed());
        Assert.assertTrue(errorInProcessName.getText().contains(expectedError));
        return this;
    }
}