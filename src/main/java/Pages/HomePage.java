package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage {

    protected WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeElement;

    @FindBy(css = ".menu-home")
    private WebElement homeMenu;

    @FindBy(linkText = "Dashboard")
    private WebElement dashboard;

    @FindBy(css = ".menu-workspace")
    private WebElement workspaceMenu;

    @FindBy(linkText = "Processes")
    private WebElement processes;

    @FindBy(linkText = "Characteristics")
    private WebElement characteristics;


    private boolean isMenuNotExpanded(WebElement menuGroup) {
        WebElement parent = menuGroup.findElement(By.xpath("./.."));
        String parentClass = parent.getAttribute("class");
        return parentClass.contains("active") == false;
    }

    public DashboardPage goToDashboardPage() {
        if (isMenuNotExpanded(homeMenu)) {
            homeMenu.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(dashboard));
        dashboard.click();
        return new DashboardPage(driver);
    }

    public ProcessesPage goToProcessesPage() {
        hardSleep();

        if (isMenuNotExpanded(workspaceMenu)) {
            workspaceMenu.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processes));
        processes.click();

        return new ProcessesPage(driver);
    }

    private void hardSleep() {
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //niestety rozwiązanie z zajęć:
        // wait.until(ExpectedConditions.visibilityOf(workspaceChildMenu));
        // spowoowało jedynie zmniejszenie częstotliwosci występowania problemu
        // z niewchodzeniem w Processes.(2 razy na 10 się wysypuje) Stąd taki toporny sleep
    }

    public CharacteristicPage goToCharacteristicPage() {
        if (isMenuNotExpanded(workspaceMenu)) {
            workspaceMenu.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(characteristics));
        characteristics.click();
        return new CharacteristicPage(driver);
    }


    public HomePage assertRegistrationProcess(String email) {
        Assert.assertTrue(welcomeElement.isDisplayed());
        Assert.assertTrue(welcomeElement.getText().contains("Welcome"));
        Assert.assertTrue(welcomeElement.getText().contains(email));
        return this;
    }

    public HomePage assertLoginProcess() {
        Assert.assertTrue(welcomeElement.isDisplayed());
        Assert.assertTrue(welcomeElement.getText().contains("Welcome"));
        return this;
    }
}