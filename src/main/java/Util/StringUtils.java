package Util;

import java.util.UUID;

public class StringUtils {

    public static String randomString(int length) {
        return UUID.randomUUID().toString().substring(0, length);
    }
}
